import models.{AppDB, Bar}
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

import org.squeryl.PrimitiveTypeMode.inTransaction

import play.api.test._
import play.api.test.Helpers._

@RunWith(classOf[JUnitRunner])
class BarSpec extends Specification{

  "A Bar" should {

    "be creatable" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        inTransaction {
          val bar = AppDB.barTable insert Bar(Some("foo"))
          bar.id must not equalTo  (0)
        }
      }
    }
  }

}
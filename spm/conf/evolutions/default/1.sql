# --- First database schema

use SPM;

# --- !Ups

create table BAR (
  id    INT NOT NULL AUTO_INCREMENT,
  name  varchar(128),
  PRIMARY KEY(id)
);


# --- !Downs

drop table BAR;
package models

import org.squeryl.{KeyedEntity, Schema}

 class Bar(val id: Long = 0, val name: String) extends KeyedEntity[Long] {
   def this() = this(0, "")
 }

object AppDB extends Schema {
  val barTable = table[Bar]("BAR")
}

name := "spm"

version := "1.0"

lazy val `spm` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq( jdbc , cache , ws   , specs2 % Test,
  "org.squeryl" % "squeryl_2.11" % "0.9.5-7",
  "mysql" % "mysql-connector-java" % "5.1.12")

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
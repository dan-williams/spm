# README #

### What is this repository for? ###
* This repository is for a Scala/Play! application using a REST API, this project is to help me learn Scala and REST and will serve as a starting point to other future projects along the same line
* Version 0.0 (no bull builds yet)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

To start set up we will need to install Play! and MySQL.
First we will set up play as follows:

* download and install play from (https://www.playframework.com/download)
* Add the Play scripts to your path (export PATH=$PATH:/relativePath/to/play)
* Check play is installed ($ play help).

Now to set up MySQL:

* Install MySQL (sudo dnf install mysql mysql-server).
* Start MySQL Service (sudo systemctl start mariadb.service).
* Set a root password (sudo /usr/bin/mysql_secure_installation) Follow the instructions you can say Yes to most of these.
* Create a new user in MySQL called "application" with password "password1!" This is the user the system will connect to MySQL with.
* If you wish so you can download MySQL work bench or DataGrip.

### Contribution guidelines ###

* Write tests for all execution paths of functions you write.
* Write full stack tests.
* All tests must be passing before being merged.
* Code must be thoroughly reviewed before merge requests are to be accepted.
* Branches must be named after the related issue.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact